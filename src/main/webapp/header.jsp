<!doctype html>
<html lang="zh-TW">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gaya System</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">

  <style>
  body {
    background-color: #FCE4EC;
    font-family:Tahoma;
  }
  .bg-main {
    background-color: #CE93D8;
  }
  .bg-second{
      background-color: #E1BEE7;
  }
   .navbar-brand{
     background-color : rgba(0, 0, 0, .01) !important;
   }
   .table {
      background-color: #D1C4E9;
   }
   .table > thead {
        background-color:#B39DDB;
   }
   .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
      background-color: #C5CAE9;
   }
   .table-striped > tbody > tr:nth-child(2n) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
         background-color: #9FA8DA;
   }
   .table-striped > thead {
        background-color: #9FA8DA;
   }
   div.card-body{
        background-color: #E1BEE7;
   }
   .form-control{
        background-color: #FFF;
   }
   .btn-primary{
        background-color: #7E57C2;
        border: #9575CD 1px solid;
   }
  </style>
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-main flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Gaya System</a>
</nav>